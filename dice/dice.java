import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;

class dice {
	/**
	* protected - only methods (functions) in this class can interact with them
	* public - any method form any class can interact with them 
	* private - only the method or class (depending on the location of the varable) can interact with them
	* static - does not move in memory (any method in a class with a main method needs to be static)
	* void - does not return any thing
	* int, String, boolean, double - if in a method header used to denote return type, otherwise it denotes a varable type
	* Scanner - data type (lets you interface with the console and/or files)
	**/
	
	protected static int numbOfDice, sumOfDice;
	protected static int numberOfCombos = 0;
	protected static Scanner stdin = new Scanner(System.in);
	protected static ArrayList<Integer> numbers = new ArrayList<Integer>();
	protected static ArrayList<Integer[]> combinations = new ArrayList<Integer[]>();
	
	public static void main(String[] args) {
		/**
		* Main Method (The entry point)
		**/
		setNumOfDice();
		setSum();
		initNumbers();
		while(!(numbers.get(0) == 7)) {
			// Recorsive loops were being killed beacuse of a heap error,
			// This was the best alternative
			addOne();
		}
		// Got lazy so I put this here
		System.out.println("Number of possible combinations: " + numberOfCombos);
		// Arrays were printing strangely so I had to write a method to print
		// them in a readable format
		printArrayList();
	}
	private static void setNumOfDice() {
		/**
		* This method is used to set the number of dice
		* and do some input sanitizing by only accepting 
		* numbers, and if a string is entered the method
		* rerun (it's recursive)
		**/
		try {
			System.out.print("Number of Dice: ");
			numbOfDice = stdin.nextInt();
		}
		
		catch(Exception err) {
			System.out.println("Input is NOT A NUMBER! ERROR(" + err + ")" );
			// The next statment is needed to stop an infinite loop of error throwing
			stdin.next();
			setNumOfDice();
		}
	}	
	private static void setSum() {
		/**
		* This method is used to set the sum
		* and do some input sanitizing by only accepting 
		* numbers, and if a string is entered the method
		* rerun (it's recursive)
		**/
		try {
			System.out.print("Sum: ");
			sumOfDice = stdin.nextInt();
		}
		
		catch(Exception err) {
			System.out.println("Input is NOT A NUMBER! ERROR(" + err + ")" );
			// The next statment is needed to stop an infinite loop of error throwing
			stdin.next();
			setSum();
		}
	}
	private static boolean equalsSum(ArrayList<Integer> nums) {
		/**
		* This method is used to determine if the sum of a given arraylist
		* is equivalent to the sum given by the user
		**/
		int total = 0;
		if(!hasSeven()) {
			for(int n=0; n < nums.size(); n++) {
				total += nums.get(n);
			}
		}
		return total == sumOfDice;
	}
	private static void initNumbers() {
		/**
		* This method sets the
		* initial length of the numbers array list
		* beacuse the rest of the code relies on 
		* the arrayList having a preset Length
		**/
		int i = 0;
		while(i++ < numbOfDice) {
			numbers.add(1);
		}
	}
	private static void addToCombinations() {
		/**
		* This method adds any posible comdos to an arraylist
		* and keeps a running total of the total combos
		*
		**/
		if(equalsSum(numbers)) {
			//DeBug
			//System.out.println(numbers);
			numberOfCombos++;
			//Needed to change arrayList into an Array B/C data was being stored improperly
			// and all the combos came out as [7,1]
			Integer[] temp = numbers.toArray(new Integer[numbers.size()]);
			combinations.add(temp);
		}
	}
	private static boolean hasSeven() {
		/**
		* The addOne method uses 7 to determine when
		* to call the setup method. This method exists to make sure
		* that any combo with a seven in it can not make it into
		* the list of posible combos
		**/
		for(int n=0; n < numbers.size(); n++) {
			if(numbers.get(n) == 7){
				return true;
			}
		}
		return false;
	}
	private static void stepUp() {
		/**
		* Place value logic for the addOne method
		**/
		int len = numbers.size() -1;
		for(int n=len; n >= 0; n--) {
			if(numbers.get(n) == 7 && n != 0) {
				numbers.set(n, 1);
				numbers.set(n-1, numbers.get(n-1) +1);
			}
		}
		addToCombinations();
		//DeBug
		//System.out.println(numbers);
	}
	private static void addOne() {
		/**
		* This method increases the
		* value of the last item in the array,
		* and makes sure that 6 is the biggest
		* number that can be stored in the last 
		* array index
		**/
		addToCombinations();
		//DeBug
		//System.out.println(numbers);
		int len = numbers.size() -1;
		if(numbers.get(len) == 7) {
			stepUp();
		}
		if(numbers.get(0) == 7) {
			return;
		}
		numbers.set(len, numbers.get(len) + 1);
	}
	private static void printArrayList() {
		/**
		* This method is used to print a
		* double array list (that looks like the out put from the pdf file)
		**/
		System.out.print("Combinations: ");
		for(int n=0; n < combinations.size(); n++) {
			System.out.print("(");
			for(int i=0; i < combinations.get(n).length; i++) {
				if(i < combinations.get(n).length -1) {
					System.out.print(combinations.get(n)[i] + ", ");
				}
				else{
					System.out.print(combinations.get(n)[i]);
				}
			}
			System.out.print(") ");
		}
		System.out.print("\n");
	}
}